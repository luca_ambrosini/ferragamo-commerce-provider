package it.deck47.commerceProvider47deck;

import com.adobe.cq.commerce.api.CommerceException;
//import com.adobe.cq.commerce.api.collection.ProductCollection;
//import com.adobe.cq.commerce.api.collection.ProductCollectionManager;
import com.adobe.cq.commerce.pim.api.ProductImporter;
import com.adobe.cq.commerce.pim.common.AbstractProductImporter;
import com.adobe.cq.commerce.pim.common.Csv;
import com.adobe.granite.workflow.launcher.ConfigEntry;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.nodetype.PropertyDefinition;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype=true, label="47Deck Product Importer", description="CSV-based product importer for 47 Deck")
@Service
@Properties({@org.apache.felix.scr.annotations.Property(name="service.description", value={"CSV-based product importer for 47Deck"}), @org.apache.felix.scr.annotations.Property(name="commerceProvider", value={"47Deck"}, propertyPrivate=true)})
public class CSVProductImporter
  extends AbstractProductImporter
  implements ProductImporter
{
  private static final Logger log = LoggerFactory.getLogger(CSVProductImporter.class);
  protected static final int OPERATION_COL = 0;
  protected static final int TYPE_COL = 1;
  protected static final int SKU_COL = 2;
  protected static final int TITLE_COL = 3;
  protected static final int VARIATION_COL = 4;
  protected static final int SIZE_COL = 5;
  protected static final int PRICE_COL = 6;
  protected static final int IMAGE_COL = 7;
  protected static final int DESCRIPTION_COL = 8;
  protected static final int TAGS_COL = 9;
  protected static final int LANGUAGE_COL = 10;
  protected static final int FIRST_CUSTOM_PROP_COL = 11;
  protected Iterator<String[]> inputIterator;
  protected boolean addToCollection;
  protected String collectionPath;
  protected final String productsRoot = "/etc/commerce/products/";
  
  protected boolean disableWorkflowPredicate(ConfigEntry workflowConfigEntry)
  {
    return workflowConfigEntry.getGlob().startsWith("/etc/commerce/products/");
  }
  
  protected boolean validateInput(SlingHttpServletRequest request, SlingHttpServletResponse response)
    throws IOException
  {
    ResourceResolver resourceResolver = request.getResourceResolver();
    
    this.collectionPath = request.getParameter("collectionPath");
    this.addToCollection = "true".equals(request.getParameter("addToCollection"));
    
    String provider = request.getParameter("provider");
    if (StringUtils.isEmpty(provider))
    {
      response.sendError(400, "No commerce provider specified.");
      return false;
    }
    String csvPath = request.getParameter("csvPath");
    InputStream is;
    try
    {
      Resource csvResource = resourceResolver.getResource(csvPath);
      Node source = (Node)csvResource.adaptTo(Node.class);
      is = source.getProperty("jcr:content/jcr:data").getBinary().getStream();
    }
    catch (Exception e)
    {
      response.sendError(500, "Product CSV [" + csvPath + "] not found.");
      return false;
    }
    String sep = request.getParameter("separator");
    String del = request.getParameter("delimiter");
    Csv csv = new Csv();
    if (sep != null) {
      csv.setFieldSeparatorRead(sep.charAt(0));
    }
    if (del != null) {
      csv.setFieldDelimiter(del.charAt(0));
    }
    this.inputIterator = csv.read(is, "utf-8");
    if (!this.inputIterator.hasNext())
    {
      response.sendError(500, "Product CSV empty.");
      return false;
    }
    this.inputIterator.next();
    if (!this.inputIterator.hasNext())
    {
      response.sendError(500, "Product CSV contains header but no products.");
      return false;
    }
    String storePath = request.getParameter("storePath");
    String storeName = request.getParameter("storeName");
    if ((StringUtils.isEmpty(storePath)) && (StringUtils.isEmpty(storeName)))
    {
      response.sendError(400, "Destination not specified.");
      return false;
    }
    return true;
  }
  
  protected void doImport(ResourceResolver resourceResolver, Node storeRoot, boolean incrementalImport)
    throws RepositoryException, IOException
  {
    String storePath = storeRoot.getPath();
    
    Map<String, String> sourceProducts = new HashMap();
    while (this.inputIterator.hasNext())
    {
      String[] cols = (String[])this.inputIterator.next();
      String op = get(cols, 0);
      String type = get(cols, 1);
      String sku = get(cols, 2);
      String productPath = getProductPath(storePath, sku);
      if (op.equals("add"))
      {
        if (type.equals("product"))
        {
          addProduct(resourceResolver, storePath, sku, cols);
          sourceProducts.put(productPath, "add");
        }
        else if (type.equals("variation"))
        {
          addVariation(resourceResolver, storePath, sku, cols);
        }
        else if (type.equals("localization"))
        {
          addLocalization(resourceResolver, storePath, sku, cols);
        }
        else if (type.equals("size"))
        {
          addSize(resourceResolver, storePath, sku, cols);
        }
        else
        {
          logMessage("ERROR unknown type: " + type, true);
          log.error("Row of unknown type: " + type);
        }
      }
      else if (op.equals("update"))
      {
        if (type.equals("product"))
        {
          updateProduct(resourceResolver, storePath, sku, cols);
          sourceProducts.put(productPath, "update");
        }
        else if ((type.equals("variation")) || (type.equals("localization")) || (type.equals("size")))
        {
          logMessage("ERROR unsupported operation: update " + type, true);
          log.error("Partial update of products not supported; update the parent product and re-add all variations and sizes");
        }
        else
        {
          logMessage("ERROR unknown type: " + type, true);
          log.error("Row of unknown type: " + type);
        }
      }
      else if (op.equals("delete"))
      {
        if (type.equals("product"))
        {
          deleteProduct(resourceResolver, storePath, sku);
          sourceProducts.put(productPath, "delete");
        }
        else if ((type.equals("variation")) || (type.equals("size")))
        {
          logMessage("ERROR unsupported operation: delete " + type, true);
          log.error("Partial update of products not supported; update the parent product and re-add all variations, localizations and sizes");
        }
        else
        {
          logMessage("ERROR unknown type: " + type, true);
          log.error("Row of unknown type: " + type);
        }
      }
      else
      {
        logMessage("ERROR unknown operation: " + op, true);
        log.error("Row of unknown type: " + op);
      }
    }
    if (this.addToCollection) {
      manageProductCollection(resourceResolver, this.collectionPath, sourceProducts);
    }
  }
  
  private void manageProductCollection(ResourceResolver resourceResolver, String collectionPath, Map<String, String> sourceProducts)
  {
    /*ProductCollectionManager collectionMgr = (ProductCollectionManager)resourceResolver.adaptTo(ProductCollectionManager.class);
    ProductCollection productCollection = collectionMgr.getCollection(collectionPath);
    if (productCollection == null)
    {
      log.error("The collection at {} is not defined.", collectionPath);
      return;
    }
    Set<String> productPaths = sourceProducts.keySet();
    for (String productPath : productPaths)
    {
      String operation = (String)sourceProducts.get(productPath);
      if (("add".equals(operation)) || ("update".equals(operation))) {
        try
        {
          productCollection.add(productPath);
          log.debug("Added product at {} to the collection {}", productPath, collectionPath);
        }
        catch (CommerceException e)
        {
          log.error("Failed to add product at {} to the collection {}", productPath, collectionPath);
        }
      } else if ("remove".equals(operation)) {
        try
        {
          productCollection.remove(productPath);
          log.debug("Removed product at {} to the collection {}", productPath, collectionPath);
        }
        catch (CommerceException e)
        {
          log.error("Failed to remove product at {} to the collection {}", productPath, collectionPath);
        }
      }
    }*/
  }
  
  protected void addProduct(ResourceResolver resourceResolver, String storePath, String sku, String[] cols)
  {
    String path = getProductPath(storePath, sku);
    try
    {
      Node productNode = createProduct(path, (Session)resourceResolver.adaptTo(Session.class));
      
      setProperties(resourceResolver, productNode, cols, null, false);
      
      createSizes(resourceResolver, productNode, cols, false);
    }
    catch (Exception e)
    {
      logMessage("ERROR creating " + path, true);
      log.error("Failed to create product " + path, e);
    }
  }
  
  protected void updateProduct(ResourceResolver resourceResolver, String storePath, String sku, String[] cols)
  {
    try
    {
      Resource product = resourceResolver.getResource(getProductPath(storePath, sku));
      if (product == null) {
        throw new RuntimeException("product doesn't exist");
      }
      Node productNode = (Node)product.adaptTo(Node.class);
      if (productNode.hasNodes())
      {
        NodeIterator it = productNode.getNodes();
        while (it.hasNext()) {
          it.nextNode().remove();
        }
      }
      setProperties(resourceResolver, productNode, cols, null, true);
      
      createSizes(resourceResolver, productNode, cols, false);
      
      productUpdated(productNode);
    }
    catch (Exception e)
    {
      logMessage("ERROR updating " + sku, true);
      log.error("Failed to update product " + sku, e);
    }
  }
  
  protected void addVariation(ResourceResolver resourceResolver, String storePath, String sku, String[] cols)
  {
    try
    {
      String parentSKU = sku.substring(0, sku.indexOf("."));
      Resource parent = resourceResolver.getResource(getProductPath(storePath, parentSKU));
      if (parent == null) {
        throw new RuntimeException("parent doesn't exist");
      }
      String variation = get(cols, 4);
      if (StringUtils.isEmpty(variation)) {
        throw new RuntimeException("no variant property specified");
      }
      String[] parts = variation.split("=", 2);
      if (parts.length != 2) {
        throw new RuntimeException("variant property syntax error");
      }
      Node variantNode = createVariant((Node)parent.adaptTo(Node.class), sku);
      variantNode.setProperty(parts[0], parts[1]);
      registerVariantAxis(variantNode, parts[0]);
      
      setProperties(resourceResolver, variantNode, cols, null, false);
      
      createSizes(resourceResolver, variantNode, cols, false);
    }
    catch (Exception e)
    {
      logMessage("ERROR adding variation " + sku, true);
      log.error("Failed to create variation " + sku, e);
    }
  }
  
  protected void addSize(ResourceResolver resourceResolver, String storePath, String sku, String[] cols)
  {
    try
    {
      Resource parent = resourceResolver.getResource(getProductPath(storePath, sku));
      if (parent == null) {
        throw new RuntimeException("parent product doesn't exist");
      }
      createSizes(resourceResolver, (Node)parent.adaptTo(Node.class), cols, true);
    }
    catch (Exception e)
    {
      logMessage("ERROR adding size " + sku, true);
      log.error("Failed to create size " + sku, e);
    }
  }
  
  protected void addLocalization(ResourceResolver resourceResolver, String storePath, String sku, String[] cols)
  {
    try
    {
      Resource parent = resourceResolver.getResource(getProductPath(storePath, sku));
      if (parent == null) {
        throw new RuntimeException("parent doesn't exist");
      }
      String language = get(cols, 10);
      if (StringUtils.isEmpty(language)) {
        throw new RuntimeException("no language specified");
      }
      setProperties(resourceResolver, (Node)parent.adaptTo(Node.class), cols, language, false);
      if (StringUtils.isNotEmpty(get(cols, 6))) {
        throw new RuntimeException("can't change price in an 'add localization' row");
      }
      if (StringUtils.isNotEmpty(get(cols, 7))) {
        throw new RuntimeException("can't change image in an 'add localization' row");
      }
      if (StringUtils.isNotEmpty(get(cols, 9))) {
        throw new RuntimeException("can't add tags in an 'add localization' row");
      }
      if (StringUtils.isNotEmpty(get(cols, 5))) {
        throw new RuntimeException("can't add sizes in an 'add localization' row");
      }
    }
    catch (Exception e)
    {
      logMessage("ERROR adding localization " + sku, true);
      log.error("Failed to add localization " + sku, e);
    }
  }
  
  protected void deleteProduct(ResourceResolver resourceResolver, String storePath, String sku)
  {
    try
    {
      Resource product = resourceResolver.getResource(getProductPath(storePath, sku));
      if (product == null) {
        throw new RuntimeException("product doesn't exist");
      }
      Node productNode = (Node)product.adaptTo(Node.class);
      
      productDeleted(productNode);
      
      productNode.remove();
    }
    catch (Exception e)
    {
      logMessage("ERROR deleting " + sku, true);
      log.error("Failed to delete product " + sku, e);
    }
  }
  
  protected void setProperties(ResourceResolver resourceResolver, Node node, String[] cols, String language, boolean update)
    throws RepositoryException
  {
    if (update)
    {
      for (PropertyIterator existingProps = node.getProperties(); existingProps.hasNext();)
      {
        Property prop = (Property)existingProps.next();
        String propName = prop.getName();
        if ((propName.startsWith("jcr:title")) || (propName.startsWith("jcr:description")) || (propName.equals("cq:tags"))) {
          prop.remove();
        } else if ((!propName.startsWith("jcr:")) && (!propName.startsWith("sling:")) && (!propName.startsWith("cq:")) && (!prop.getDefinition().isAutoCreated()) && (!prop.getDefinition().isProtected())) {
          prop.remove();
        }
      }
      if (node.hasNode("image")) {
        node.getNode("image").remove();
      }
      if (node.hasNode("assets")) {
        node.getNode("assets").remove();
      }
    }
    String title = get(cols, 3);
    if (StringUtils.isNotEmpty(title))
    {
      String name = "jcr:title";
      if (language != null) {
        name = name + "." + language;
      }
      node.setProperty(name, title);
    }
    String sku = get(cols, 2);
    if (StringUtils.isNotEmpty(sku)) {
      node.setProperty("identifier", sku);
    }
    String price = get(cols, 6);
    if (StringUtils.isNotEmpty(price)) {
      node.setProperty("price", new BigDecimal(price));
    }
    String imageUrl = get(cols, 7);
    if (StringUtils.isNotEmpty(imageUrl))
    {
      String[] split = imageUrl.split(",");
      createAssets(node, split);
    }
    String description = get(cols, 8);
    if (StringUtils.isNotEmpty(description))
    {
      String name = "jcr:description";
      if (language != null) {
        name = name + "." + language;
      }
      node.setProperty(name, description);
    }
    String tagCol = get(cols, 9);
    if (StringUtils.isNotEmpty(tagCol))
    {
      if (!node.isNodeType("cq:Taggable")) {
        node.addMixin("cq:Taggable");
      }
      String[] tags = tagCol.split(",");
      createMissingTags(resourceResolver, tags);
      node.setProperty("cq:tags", tags);
    }
    for (int i = 11; i < cols.length; i++)
    {
      String customPropDefinition = cols[i];
      if (StringUtils.isNotEmpty(customPropDefinition))
      {
        String[] parts = customPropDefinition.split("=", 2);
        String name = parts[0].trim();
        if ((parts.length != 2) || (!mangleName(name).equals(name)))
        {
          logMessage("ERROR adding custom property: syntax error in property definition", true);
          log.error("Custom property syntax error");
        }
        else
        {
          if (language != null) {
            name = name + "." + language;
          }
          node.setProperty(name, parts[1]);
        }
      }
    }
  }
  
  protected void createAssets(Node productNode, String[] assets)
    throws RepositoryException
  {
    if (assets == null) {
      return;
    }
    List<String> assetsList = new ArrayList();
    for (String url : assets) {
      if (StringUtils.isNotBlank(url)) {
        assetsList.add(url.trim());
      }
    }
    if (assetsList.isEmpty()) {
      return;
    }
    Node imageNode = createImage(productNode);
    imageNode.setProperty("fileReference", (String)assetsList.get(0));
    if (assetsList.size() > 1)
    {
      Node assetsNode = productNode.addNode("assets", "nt:unstructured");
      for (int i = 1; i < assetsList.size(); i++)
      {
        String nodeName = "asset";
        if (i > 1) {
          nodeName = nodeName + (i - 2);
        }
        Node assetNode = assetsNode.addNode(nodeName, "nt:unstructured");
        assetNode.setProperty("sling:resourceType", "commerce/components/product/image");
        assetNode.setProperty("jcr:lastModified", Calendar.getInstance());
        assetNode.setProperty("fileReference", (String)assetsList.get(i));
        
        logMessage("Created asset     " + assetNode.getPath(), false);
        checkpoint(assetsNode.getSession(), false);
      }
      Node baseProduct = getBaseProduct(productNode);
      if (baseProduct != null) {
        logEvent("com/adobe/cq/commerce/pim/PRODUCT_MODIFIED", baseProduct.getPath());
      }
    }
  }
  
  protected void createSizes(ResourceResolver resourceResolver, Node node, String[] cols, boolean fromAddSizeCommand)
  {
    String sizeInfo = get(cols, 5);
    if (StringUtils.isNotEmpty(sizeInfo))
    {
      String[] sizes = sizeInfo.split(",");
      for (String size : sizes) {
        try
        {
          Node variant = createVariant(node, "size-" + mangleName(size));
          variant.setProperty("size", size.trim());
          if (fromAddSizeCommand) {
            setProperties(resourceResolver, variant, cols, null, false);
          }
        }
        catch (Exception e)
        {
          logMessage("ERROR creating size " + size, true);
          log.error("Failed to create size " + size, e);
        }
      }
      registerVariantAxis(node, "size");
    }
  }
  
  private String get(String[] cols, int i)
  {
    if (i < cols.length) {
      return cols[i];
    }
    return null;
  }
  
  protected void registerVariantAxis(Node product, String axis)
  {
    try
    {
      while (product.getProperty("cq:commerceType").getString().equals("variant")) {
        product = product.getParent();
      }
      String[] axes;
      if (product.hasProperty("cq:productVariantAxes"))
      {
        Value[] values = product.getProperty("cq:productVariantAxes").getValues();
        axes = new String[values.length + 1];
        for (int i = 0; i < values.length; i++)
        {
          axes[i] = values[i].getString();
          if (axes[i].equals(axis)) {
            return;
          }
        }
        axes[values.length] = axis;
      }
      else
      {
        axes = new String[1];
        axes[0] = axis;
      }
      product.setProperty("cq:productVariantAxes", axes);
    }
    catch (RepositoryException e)
    {
      log.error("Failed to register variant axis " + axis, e);
    }
  }
  
  protected String getProductPath(String storePath, String sku)
  {
    String productSKU = sku;
    boolean variation = false;
    if (sku.contains("."))
    {
      productSKU = sku.substring(0, sku.indexOf("."));
      variation = true;
    }
    String path = storePath + "/" + sku.substring(0, 2) + "/" + sku.substring(0, 4) + "/" + productSKU;
    if (variation) {
      path = path + "/" + sku;
    }
    return path;
  }
}