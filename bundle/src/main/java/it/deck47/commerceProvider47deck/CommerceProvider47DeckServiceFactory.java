package it.deck47.commerceProvider47deck;

import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.CommerceServiceFactory;
import com.adobe.cq.commerce.common.AbstractJcrCommerceServiceFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;

@Component
@Service
@Properties({@org.apache.felix.scr.annotations.Property(name="service.description", value={"47Deck Factory for reference implementation commerce service"}), @org.apache.felix.scr.annotations.Property(name="commerceProvider", value={"47Deck_Commerce_Provider"}, propertyPrivate=true)})
public class CommerceProvider47DeckServiceFactory
  extends AbstractJcrCommerceServiceFactory
  implements CommerceServiceFactory
{
  public CommerceService getCommerceService(Resource res)
  {
    return new CommerceProvider47DeckServiceImpl(getServiceContext(), res); 
  }
}