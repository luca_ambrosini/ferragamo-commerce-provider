package it.deck47.commerceProvider47deck;

import com.adobe.cq.commerce.common.AbstractJcrProduct;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
//import info.geometrixx.commons.util.GeoHelper;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

public class CommerceProvider47DeckProductImpl
  extends AbstractJcrProduct
{
  public static final String PN_IDENTIFIER = "identifier";
  public static final String PN_PRICE = "price";
  protected final ResourceResolver resourceResolver;
  protected final PageManager pageManager;
  protected final Page productPage;
  protected String brand = null;
  
  public CommerceProvider47DeckProductImpl(Resource resource)
  {
    super(resource);
    
    this.resourceResolver = resource.getResourceResolver();
    this.pageManager = ((PageManager)this.resourceResolver.adaptTo(PageManager.class));
    this.productPage = this.pageManager.getContainingPage(resource);
  }
  
  public String getSKU()
  {
    String sku = (String)getProperty("identifier", String.class);
    
    String size = (String)getProperty("size", String.class);
    if ((size != null) && (size.length() > 0)) {
      sku = sku + "-" + size;
    }
    return sku; 
  }
  
  /*public <String> String getProperty(String name, Class<String> type)
  {
    if (name.equals("brand")) {
      return getBrand();
    }
    return (String)super.getProperty(name, type);
  }
  
  public <String> String getProperty(String name, String selectorString, Class<String> type)
  {
    if (name.equals("brand")) {
      return getBrand();
    }
    return (String)super.getProperty(name, selectorString, type);
  }
  
  public String getBrand()
  {
    if (this.brand == null)
    {
     if (this.productPage != null) {
        this.brand = GeoHelper.getPageTitle(this.productPage.getAbsoluteParent(2));
      }
      if (GeoHelper.isEmpty(this.brand)) {
        this.brand = "";
      }
    }
    return this.brand;
  }*/
}
