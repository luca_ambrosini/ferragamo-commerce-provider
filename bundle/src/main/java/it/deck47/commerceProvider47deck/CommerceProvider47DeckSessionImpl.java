package it.deck47.commerceProvider47deck;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.PlacedOrder;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.AbstractJcrCommerceSession;
import com.adobe.cq.commerce.common.ServiceContext;
import com.day.cq.i18n.I18n;
import java.math.BigDecimal;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import org.apache.commons.collections.Predicate;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;

public class CommerceProvider47DeckSessionImpl
  extends AbstractJcrCommerceSession
{
  public CommerceProvider47DeckSessionImpl(AbstractJcrCommerceService commerceService, SlingHttpServletRequest request, SlingHttpServletResponse response, Resource resource)
    throws CommerceException 
  {
    super(commerceService, request, response, resource);
    this.PN_UNIT_PRICE = "price";
  }
  
  protected BigDecimal getShipping(String method)
  {
    String[][] shippingCosts = { { "/etc/commerce/shipping-methods/geometrixx-outdoors/ground", "10.00" }, { "/etc/commerce/shipping-methods/geometrixx-outdoors/three-day", "20.00" }, { "/etc/commerce/shipping-methods/geometrixx-outdoors/two-day", "25.00" }, { "/etc/commerce/shipping-methods/geometrixx-outdoors/overnight", "40.00" } };
    for (String[] entry : shippingCosts) {
      if (entry[0].equals(method)) {
        return new BigDecimal(entry[1]);
      }
    }
    return BigDecimal.ZERO;
  }
  
  protected String tokenizePaymentInfo(Map<String, String> paymentDetails)
    throws CommerceException
  {
    return "faux-payment-token";
  }
  
  protected void initiateOrderProcessing(String orderPath)
    throws CommerceException
  {}
  
  protected String getOrderStatus(String orderId)
    throws CommerceException
  {
    Session serviceSession = null;
    try
    {
      serviceSession = this.commerceService.serviceContext().slingRepository.loginService("orders", null);
      
      StringBuilder buffer = new StringBuilder();
      buffer.append("/jcr:root/etc/commerce/orders//element(*)[@orderId = '").append(Text.escapeIllegalXpathSearchChars(orderId).replaceAll("'", "''")).append("']");
      
      Query query = serviceSession.getWorkspace().getQueryManager().createQuery(buffer.toString(), "xpath");
      NodeIterator nodeIterator = query.execute().getNodes();
      if (nodeIterator.hasNext()) {
        return nodeIterator.nextNode().getProperty("orderStatus").getString();
      }
    }
    catch (Exception e)
    {
      log.error("Error while fetching order status for orderId '" + orderId + "'", e);
    }
    finally
    {
      if (serviceSession != null) {
        serviceSession.logout();
      }
    }
    I18n i18n = new I18n(this.request);
    return i18n.get("unknown", "order status");
  }
  
  protected Predicate getPredicate(String predicateName)
  {
    if ((predicateName != null) && (predicateName.equals("openOrders"))) {
      new Predicate()
      {
        public boolean evaluate(Object object)
        {
          try
          {
            PlacedOrder order = (PlacedOrder)object;
            String status = (String)order.getOrder().get("orderStatus");
            return (status != null) && (!status.equals("Completed")) && (!status.equals("Cancelled"));
          }
          catch (CommerceException e) {}
          return false;
        }
      };
    }
    return null;
  }
}
