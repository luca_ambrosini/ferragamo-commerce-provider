package it.deck47.commerceProvider47deck;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.CommerceSession;
//import com.adobe.cq.commerce.api.PaymentMethod;
import com.adobe.cq.commerce.api.Product;
//import com.adobe.cq.commerce.api.ShippingMethod;
import com.adobe.cq.commerce.api.promotion.Voucher;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.adobe.cq.commerce.common.ServiceContext;
import com.adobe.cq.commerce.common.promotion.AbstractJcrVoucher;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.Property;
import org.apache.commons.collections.Predicate;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;

public class CommerceProvider47DeckServiceImpl
  extends AbstractJcrCommerceService
  implements CommerceService
{
  private Resource resource;
  
  public CommerceProvider47DeckServiceImpl(ServiceContext serviceContext, Resource resource)
  {
    super(serviceContext, resource);
    this.resource = resource;
  }
  
  /*public CommerceSession login(SlingHttpServletRequest request, SlingHttpServletResponse response)
    throws CommerceException 
  {
    return new CommerceProvider47DeckSessionImpl(this, request, response, this.resource);
  }*/
  
  public boolean isAvailable(String serviceType)
  {
    if ("commerce-service".equals(serviceType)) {
      return true;
    }
    return false;
  }
  
  public Product getProduct(String path)
    throws CommerceException
  {
    Resource resource = this.resolver.getResource(path);
    /*if ((resource != null) && (GeoProductImpl.isAProductOrVariant(resource))) {
      return new GeoProductImpl(resource);
    }*/
    return null;
  }
  
  public Voucher getVoucher(String path)
    throws CommerceException
  {
    Resource resource = this.resolver.getResource(path);
    if (resource != null)
    {
      Resource contentResource = resource.getChild("jcr:content");
      if ((contentResource != null) && (contentResource.isResourceType("commerce/components/voucher"))) {
        return new AbstractJcrVoucher(resource);
      }
    }
    return null;
  }
  
  public void catalogRolloutHook(Page blueprint, Page catalog) {}
  
  public void sectionRolloutHook(Page blueprint, Page section) {}
  
  public void productRolloutHook(Product productData, Page productPage, Product product)
    throws CommerceException
  {
    try
    {
      boolean changed = false;
      
      Node productNode = (Node)product.adaptTo(Node.class);
      if (productData.axisIsVariant("color"))
      {
        if (!productNode.hasProperty("variationAxis"))
        {
          productNode.setProperty("variationAxis", "color");
          productNode.setProperty("variationTitle", "Color");
          changed = true;
        }
      }
      else if ((productNode.hasProperty("variationAxis")) && (productNode.getProperty("variationAxis").getString().equals("color")))
      {
        productNode.setProperty("variationAxis", "");
        productNode.setProperty("variationTitle", "");
        changed = true;
      }
      if (CommerceHelper.copyTags(productData, productPage.getContentResource(), new Predicate()
      {
        public boolean evaluate(Object o)
        {
          return ((Tag)o).getNamespace().getName().equals("geometrixx-outdoors");
        }
      })) {
        changed = true;
      }
      if (!ResourceUtil.isA(productPage.getContentResource(), "commerce/components/productpageproxy"))
      {
        String productImageRef = "";
        Resource productImage = productData.getImage();
        if (productImage != null) {
          productImageRef = (String)ResourceUtil.getValueMap(productImage).get("fileReference", "");
        }
        Node contentNode = (Node)productPage.getContentResource().adaptTo(Node.class);
        Node pageImageNode = JcrUtils.getOrAddNode(contentNode, "image", "nt:unstructured");
        pageImageNode.setProperty("fileReference", productImageRef);
      }
      if (changed) {
        productPage.getPageManager().touch((Node)productPage.adaptTo(Node.class), true, Calendar.getInstance(), false);
      }
    }
    catch (Exception e)
    {
      throw new CommerceException("Product rollout hook failed: ", e);
    }
  }
  
  /*public List<ShippingMethod> getAvailableShippingMethods()
    throws CommerceException
  {
    return enumerateMethods("/etc/commerce/shipping-methods/geometrixx-outdoors", ShippingMethod.class);
  }*/
  
  /*public List<PaymentMethod> getAvailablePaymentMethods()
    throws CommerceException
  {
    return enumerateMethods("/etc/commerce/payment-methods/geometrixx-outdoors", PaymentMethod.class);
  }*/
  
  public List<String> getCountries()
    throws CommerceException
  {
    List<String> countries = new ArrayList();
    
    countries.add("*");
    
    return countries;
  }
  
  public List<String> getCreditCardTypes()
    throws CommerceException
  {
    List<String> ccTypes = new ArrayList();
    
    ccTypes.add("*");
    
    return ccTypes;
  }
  
  public List<String> getOrderPredicates()
    throws CommerceException
  {
    List<String> predicates = new ArrayList();
    predicates.add("openOrders");
    return predicates;
  }

public CommerceSession login(SlingHttpServletRequest arg0,
		SlingHttpServletResponse arg1) throws CommerceException {
	// TODO Auto-generated method stub
	return null;
}
}